#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Martin'
AUTHOR_FULL = 'Dr. Martin Wetzel'
AUTHORDESCRIPTION = 'Sociologist and data scientist working at the University of Cologne.'
SITENAME = "Martin's projects"
SITEDESCRIPTION = 'Welcome to my showroom.'
#SITEURL = 'b-dx.gitlab.io'
SITEURL = 'localhost:8000'
LOGO = 'images/logo.png'
OUTPUT_PATH = 'public/'

# Plugins
PLUGIN_PATHS = ['theme/plugins/',]
PLUGINS = ['i18n_subsites', 'tipue_search']
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}


THEME = 'theme/pelican-fh5co-marble-mw/'
STATIC_PATHS = ['images', 'pages', ]
FAVICON = 'images/favicon.ico'
#THEME_STATIC_PATHS = ['static/css', 'static', 'static/css/fonts']
#THEME_STATIC_PATHS = ['pelican-fh5co-marble2',]

# content paths
PATH = 'content'
#PAGE_PATHS = ['pages/en']
#ARTICLE_PATHS = ['blog/en']
#ARTICLE_PATHS = STATIC_PATHS
#PAGE_PATHS = STATIC_PATHS


# some settings
TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'en'
DEFAULT_DATE_FORMAT = '%a, %d %b %Y'
DEFAULT_PAGINATION = 10
# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
FILENAME_METADATA = '(?P<date>\d{4}-\d{2}-\d{2})-(?P<slug>.*)'
HEADERIMAGE = 'images/gears2_small.jpg'

# Feed generation is usually not desired when developing
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
RSS_FEED_SUMMARY_ONLY = False

# Social widget
SOCIAL = (
  ('gitlab', 'https://www.b-dx.gitlab.io'),
  ('iss', 'http://www.iss-wiso.uni-koeln.de/de/institut/personen/w/dr-martin-wetzel/'),
  ('google', 'https://scholar.google.de/citations?user=VIkbBqAAAAAJ&hl=de&oi=sra'),
  ('rss', 'https://b-dx.gitlab.io/feeds/all.rss.xml'),
)

ABOUT = {
  'image': 'images/avatar.jpg',
  'text': 'If you have questions or comments - please contact me!.',
  'link': 'contact.html',
}
 # 'address': 'Berlin - Wedding, Germany',

# navigation and homepage options
DISPLAY_PAGES_ON_MENU = True
DISPLAY_PAGES_ON_HOME = False
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_TAGS_ON_MENU = False
USE_FOLDER_AS_CATEGORY = True
PAGE_ORDER_BY = 'order'

MENUITEMS = [
  ('Archive', 'archives.html'),
  ('Contact', 'contact.html')
]

DIRECT_TEMPLATES = [
  'index',
  'tags',
  'categories',
  'authors',
  'archives',
  'search', # needed for tipue_search plugin
  'contact' # needed for the contact form
]

# setup google maps
#GOOGLE_MAPS_KEY = 'AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA'
