# My startpage
Using pelican.


## To-Do

* Fill with content!
* In archive does not work yet

### It was a pain in the ass to get it running.

Pelican was nice to use. I finally forked a gitlab-project using pelican, git cloned it, then added the nicer pelican-project I had already prepaired, commited that then I tried to find the right settings for the git user page to appear under b-dx.gitlab.io. Here this [tutorial](https://www.youtube.com/watch?v=TWqh9MtT4Bg) helped. First start ci/cd with "jobs", then rename project with b-dx.gitlab.io, then run jobs again - and hopefully you are back on track.

I am curious how this will go on when I change the template, add entries and try to link to my "public" gitlab projects.

## Pelican
`pelican-quickstart`

Remember to use automatization.




Sources:
  * [Pelican Docs](http://docs.getpelican.com/en/stable)
  * [Renata's Page](https://rsip22.github.io/blog/create-a-blog-with-pelican-and-github-pages.html)
  * [Segfaulted](http://segfaulted.com/pelican-on-gitlab-pages.html)
