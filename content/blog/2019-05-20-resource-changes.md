Title: NEW: Level and Change in Economic, Social, and Personal Resources for People Retiring
Category: Academia
Image: images/gr_res.jpg
Tags: retirement, stata
summary: When people retire they do not only lose resources. The pattern is more complex and depends on the last labor market status.

# Level and Change in Economic, Social, and Personal Resources for People Retiring from Paid Work and Other Labour Market Statuses

This was joined work with [Catherine E. Bowen](https://www.researchgate.net/profile/Catherine_Bowen2) and [Oliver Huxhold](https://www.dza.de/en/about-the-dza/staff.html?tx_dzastaff_view%5Baction%5D=show&tx_dzastaff_view%5Bcontroller%5D=Employee&tx_dzastaff_view%5Bemployee%5D=17&cHash=60eee9866fee1831ad8e9389d82afca4). You find the article [here](http://link.springer.com/article/10.1007/s10433-019-00516-y). It was published in *European Journal of Ageing* in 2019.

**You find the code [here](https://gitlab.com/B-DX/resource-changes-with-retirement) in my gitlab repository.**

__Abstract__

Well-being in retirement is thought to depend on person's level of resources and how his or her resources change during retirement. However, to date few studies have directly investigated resource trajectories during retirement. The current study therefore examines how economic, personal, and social-relational resources change during the retirement transition for people retiring from paid employment and for people retiring from other, non-working labour market statuses (e.g., disability pension, homemaker, unemployment).

Based on four representative baseline samples of the German Ageing Survey (1996, 2002, 2008, 2014) and their respective six-year follow-up interviews, we identified N=586 retirees. We then used latent change score models to separately estimate the level and change in income, health, activity, family and non-family network size, and social support for people retiring from paid work (n=384) and people retiring from other statuses (n=202) adjusted for age, gender, education, region, period, and time since retirement.

High autoregression coefficients indicated that resources largely remained stable during retirement. Income and social support declined and family networks increased both for those retiring from paid work and those retiring from other statuses. Leisure activities increased only for those retiring from paid work. No changes in health or nonfamily networks were observed.

Overall, we found that resources changed only modestly during the retirement transition. Resource changes did, however, differ by last labour market status and socio-demographic characteristics. Overall, people with many resources before retirement also have many resources after retirement. We conclude that retirement affects resources less than researchers often expect.
