Title: Happybeesfields: A Django-app with leaflet deployed on heroku
Category: Miscellaneous
Image: images/gr_hbf.jpg
Tags: python, django
summary: Where does your honey come from? HBF could give you personalized information.

# A prototype of a Django-app with open-source map-integration deployed on heroku

I was curious if I could program the backend for an app which gives you individualized information from a database accessed over an random url. The answer is: yes, I can. It is not pretty (yet) but it works just fine as herokuapp [here](https://happybeesfields.herokuapp.com/).

**You find the code [here](https://gitlab.com/B-DX/happybeesfields) in my gitlab repository.**

Everything as CI/CD-automized meaning that with each new push on gitlab heroku will update the app.

__What I learned on the way:__

* This is my third try of creating a Django-app and I felt much more secure on the way programing it. Nice.
* I wanted to use Docker as environment this time. This took some time to set it up nicely but then it worked. However, for pushing the results on a gitlab-runner, I realized that pushing a docker into a gitlab-docker can be done but seems not to have any advantages. Accordingly, I let Docker go for deployment - at least on my end and also started to use for development the easy-to-run "venv".
* But the hardest time gave me heroku. It took me hours to figure out on which layer of my folders the "Procfile" and the "wsgi.py" needed to be found. There was suprisingly little help to find in the net and I often stumpled over unanswered questions in stackoverflow. But finally I figured it out and now I have a nice basis for upcoming projects.

__What helped alot:__

* [Simpleisbetterthancomplex.com](Simpleisbetterthancomplex.com) is such a great tutorial series on Django and related topic, I cann't strech enough how helpful this page is. In particular the ["Complete beginners guide on Django"](https://simpleisbetterthancomplex.com/series/beginners-guide/1.11/) is highly recommendable and also the part of ["deploying Django on heroku"](https://simpleisbetterthancomplex.com/tutorial/2016/08/09/how-to-deploy-django-applications-on-heroku.html) helped alot but left some open questions.
* For setting up the Django I also liked watching the London App Developer with his tutorial on youtube [here](https://www.youtube.com/watch?v=KN8wuFi2RXM) and [here](https://www.youtube.com/watch?v=Y_rh-VeC_j4).
