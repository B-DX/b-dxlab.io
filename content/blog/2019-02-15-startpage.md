Title: My new homepage
Category: Miscellaneous
Image: images/gears2_teaser.jpg
Tags: python
Order: 2
summary: I launched my personal page on gitlab!

### Hello world!

I finally started to publish systematically the codes for the projects I do in my working but also in my spare time. This page builts a nice basis and is also the first project! Check out the code here.

I used [pelican](http://docs.getpelican.com/) - a python-based static site generator tool - as the back-end and the [fh5co-marble theme](https://github.com/claudio-walser/pelican-fh5co-marble.git) that it looks pretty. I made some small adjustments so that it better fits my needs. What I like the most is that using the CI/CD-engine from gitlab allows me easily to add new posts while all links in the background will updated automatically!

And now I am ready to stepwise add stepwise first the code of all the projects I did so far and then later I will publish my most recent projects here too.

##### Things to To-Do
* tipuesearch doesn't work yet
* logo on about-page is missing
* several typos all over the place
* but most importantly: add content!
