Title: About me
Date: 1984-12-03

I am a sociologist working at the University of Cologne (Germany) with a strong interest in peoples biographies and data scientist. I learned in academia to be analytical which helps to define needs first before starting to survey or to analyse. I have a strong command in preparing data - I love to automate - which helps of course to reproduce findings. But mostly I like the first moments of an analysis when your model runs through and you have this light bulb moment and everything becomes clear. I think science is nothing you should do alone. Accordingly, I like to talk about findings, depending on the audience this can be elaborate, nerdy or just entertaining.

I like the idea of the open-source community that openly shared knowledge helps to progress faster. Accordingly, I try to share as much of my code for my work or my hobbies here at [gitlab](/archives.html). I believe in the slogan “public money, public code” meaning that in particular people working in the public sector (hello scientists!) should try to publish their work as open source as the public has funded it already. If anyone has problems accessing my code or my publications, [please contact me](/contact.html).

![Where I come from]({static}/images/wedding.jpg)
